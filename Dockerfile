FROM python:3-alpine
LABEL MAINTAINER Naveen Kumar Sangi <naveenkumarsangi@protonmail.com>

RUN mkdir -p /usr/src/app
COPY . /usr/src/app

RUN apk add --no-cache git

WORKDIR  /usr/src/app

RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt
