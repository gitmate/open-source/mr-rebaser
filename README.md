# MergeRequest Rebaser

This repository contains the source code for building a docker image to perform
git operations on merge requests like merge, rebase, squash etc.

## Supported Commands

- rebase
- fastforward or ff
- merge
- squash

## How to use

- Rebasing `some-branch` on top of `master`:

```bash
$ python run.py rebase https://someauthtoken@hoster.com/nkprince007/test.git https://someauthtoken@hoster.com/test-user/test.git some-branch master
```

- Merging `some-branch` with `master`:

```bash
$ python run.py merge https://someauthtoken@hoster.com/nkprince007/test.git
https://someauthtoken@hoster.com/test-user/test.git some-branch master
```

- Squashing all commits in `some-branch` which are ahead of `master` into one
  commit with commit message as `message`:

```bash
$ python run.py squash https://someauthtoken@hoster.com/nkprince007/test.git
https://someauthtoken@hoster.com/test-user/test.git some-branch master
message
```

## Steps to build the docker image

```bash
$ docker build -t mr_rebaser .
```

This step creates a docker image with the name **mr_rebaser** that can rebase a
branch of repository over another and push the changes.
