import os
from subprocess import CalledProcessError
from shutil import rmtree
from tempfile import mkdtemp
from unittest import TestCase

from requests import delete
from requests import post

from run import merge
from run import run_command
from run import CommandStatus


class MergeTestCase(TestCase):
    def setUp(self):
        self.head_repo_test_token = os.environ['HEAD_REPO_TEST_TOKEN']
        self.base_repo_test_token = os.environ['BASE_REPO_TEST_TOKEN']
        self.head_repo_id = os.environ['HEAD_REPO_ID']
        self.base_repo_id = os.environ['BASE_REPO_ID']
        self.head_url = os.environ['HEAD_URL']
        self.base_url = os.environ['BASE_URL']
        try:
            self.test_path = mkdtemp()
            self.addCleanup(rmtree, self.test_path)
            commands = (
                f'git clone --quiet {self.head_url} {self.test_path}',
                f'git remote set-url origin {self.head_url}',
                f'git remote add upstream {self.base_url}',
                f'git fetch upstream master',
                f'git checkout --quiet master~1',
                f'git checkout --quiet -b new-branch',
                f'git push --quiet -u {self.head_url} new-branch',
            )
            for command in commands:
                run_command(command, self.test_path)

            # creating a new merge request
            self.mr_data = post(
                f'https://gitlab.com/api/v4/projects/{self.head_repo_id}/'
                f'merge_requests',
                json={
                    'title': 'test mr merge',
                    'source_branch': 'new-branch',
                    'target_branch': 'master',
                    'target_project_id': 3439658
                },
                params={'access_token': self.head_repo_test_token}).json()
        except (CalledProcessError, ValueError) as ex:  # pragma: no cover
            print(f'ERROR: {str(ex)}')
            return

    def test_command(self):
        status, error = merge(
            self.head_url, self.base_url, 'new-branch', 'master')
        self.assertEqual(status, CommandStatus.SUCCESS)
        self.assertIsNone(error)

    def tearDown(self):
        try:
            delete((f'https://gitlab.com/api/v4/projects/{self.base_repo_id}/'
                    f"merge_requests/{self.mr_data['iid']}"),
                   params={'access_token': self.base_repo_test_token})
            command = 'git push --quiet origin :new-branch'
            run_command(command, self.test_path)
        except CalledProcessError as ex:  # pragma: no cover
            print(f'ERROR: {str(ex)}')
